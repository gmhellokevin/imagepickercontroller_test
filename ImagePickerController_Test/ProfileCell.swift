//
//  ProfileCell.swift
//  ImagePickerController_Test
//
//  Created by Kevin.CviCloud on 2020/2/20.
//  Copyright © 2020 Kevin.CviCloud. All rights reserved.
//

import UIKit


protocol ProfileCellDelegate {
    func setProfileImage(cell:ProfileCell)
    func setUserName(cell:ProfileCell)
}

class ProfileCell: UITableViewCell , UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    var delegate:ProfileCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // ImageView
        self.profileImage.layer.borderWidth = 1
        self.profileImage.layer.borderColor = UIColor.white.cgColor
        self.profileImage.contentMode = .scaleAspectFill
        self.profileImage.clipsToBounds = true 
        self.profileImage.image = UIImage(named: "user")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGesture:)))
        self.profileImage.isUserInteractionEnabled = true
        self.profileImage.addGestureRecognizer(tapGestureRecognizer)
        
        // Label
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(tapGesture:)))
        self.name.isUserInteractionEnabled = true
        self.name.addGestureRecognizer(tapGestureRecognizer2)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func imageTapped(tapGesture:UITapGestureRecognizer) {
        self.delegate?.setProfileImage(cell: self)
    }
    @objc func labelTapped(tapGesture:UITapGestureRecognizer) {
        self.delegate?.setUserName(cell: self)
    }
    
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var image:UIImage? = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        if image == nil {
            image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        }
        self.profileImage.image = image
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
}
