//
//  ViewController.swift
//  ImagePickerController_Test
//
//  Created by Kevin.CviCloud on 2020/2/20.
//  Copyright © 2020 Kevin.CviCloud. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "profileCell")
    }


}

extension ViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        let profileCell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileCell
        profileCell.profileImage.layer.cornerRadius = profileCell.profileImage.frame.height/2
        profileCell.delegate = self
        
        cell = profileCell
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    // 必需有輸入
    @objc func alertTextFieldDidChange(notification:NSNotification) {
        
        if let alertController = self.presentedViewController as? UIAlertController {
            let name = alertController.textFields?.last
            
            let okAction = alertController.actions.last
            okAction?.isEnabled = ((name?.text?.count)! > 0)
        }
    }
}

extension ViewController : ProfileCellDelegate {
    
    func setProfileImage(cell: ProfileCell) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            imagePicker.delegate = cell
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func setUserName(cell:ProfileCell) {
        self.renameAlert(cell: cell)
    }
    
    // 更名
    func renameAlert(cell:ProfileCell){
        
        let alert = UIAlertController(title: "請輸入姓名", message: "", preferredStyle: .alert )
        let doneAction:UIAlertAction = UIAlertAction(title: "確認", style: .default) { action -> Void in
            let newName = (alert.textFields?.last)?.text!
            cell.name.text = newName
            NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
        }
        
        let cancelAction:UIAlertAction = UIAlertAction(title: "取消", style: .cancel) { action -> Void in
            print("取消")
            NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
        }
        
        alert.addTextField { (textField) in
            textField.placeholder = cell.name.text
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.alertTextFieldDidChange(notification:)), name: UITextField.textDidChangeNotification, object: textField)
            
            if (textField.text?.count)! > 0 {
                cell.name.text = textField.text
            }else {
                doneAction.isEnabled = false
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(doneAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

